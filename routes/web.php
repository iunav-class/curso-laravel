<?php

use App\Http\Controllers\coursecontroller;
use App\Http\Controllers\cursoscontroller;
use App\Http\Controllers\homecontroller;
use Illuminate\Support\Facades\Route;


Route::get('/', homecontroller::class);

route::controller(cursoscontroller::class)->group(function(){
    route::get('cursos','index');
    route::get('cursos/create','create');
    route::get('cursos/{curso}', 'show');

});