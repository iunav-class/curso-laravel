public function show(Post $post)
{
    $comentarios = $post->comentarios()->get();

    return view('post.show', compact('post', 'comentarios'));
}
