public function store(Request $request, Post $post)
{
    $comentario = new Comentario($request->all());
    $comentario->post_id = $post->id;
    $comentario->save();

    return redirect()->back();
}
s